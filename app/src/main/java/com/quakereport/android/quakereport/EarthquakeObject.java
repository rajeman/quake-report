package com.quakereport.android.quakereport;

import static android.R.attr.name;

/**
 * Created by ROJO on 14-Apr-18.
 */

public class EarthquakeObject {

    private String location;
    private String magnitude;
    private Long timeStamp;
    private String url;


    public EarthquakeObject(String location, String magnitude, Long timeStamp, String url) {
        this.location = location;
        this.magnitude = magnitude;
        this.timeStamp = timeStamp;
        this.url = url;

    }

    public String getLocation() {
        return location;
    }

    public String getMagnitude() {
        return magnitude;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public String getUrl() {
        return url;
    }
    @Override
    public String toString(){

        return location + magnitude;
    }
}
