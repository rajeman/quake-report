package com.quakereport.android.quakereport;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import static android.R.attr.key;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {


    public SettingsFragment() {
        // Required empty public constructor
    }


   @Override
   public void onCreate(Bundle savedInstanceState){
       super.onCreate(savedInstanceState);
       addPreferencesFromResource(R.xml.settings_main);
        setCurrentSummary(); //updates the preferences summary to the current values

   }


    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("order_by")) {
            Preference connectionPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            connectionPref.setSummary(getString(R.string.order_by_prefix) +" " + sharedPreferences.getString(key, "time" ) );
        }
        else if(key.equals("max_items")){

            Preference connectionPref = findPreference(key);

            // Set summary to be the user-description for the selected value
            connectionPref.setSummary(sharedPreferences.getString(key, "" ) + " " + getString(R.string.limit_summary_suffix)  );

        }
    }

    @Override
    public void onResume(){

        super.onResume();

        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause(){

        super.onPause();

        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);

    }

    public void setCurrentSummary(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        Preference maxItems = findPreference(getString(R.string.limit_key));
       maxItems.setSummary(preferences.getString(getString(R.string.limit_key), "" ) + " " + getString(R.string.limit_summary_suffix) );

        Preference orderBy = findPreference(getString(R.string.order_by_key));
        orderBy.setSummary(getString(R.string.order_by_prefix) +" " + preferences.getString(getString(R.string.order_by_key), "" ) );

    }
}
