package com.quakereport.android.quakereport;

import android.content.SharedPreferences;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class SettingsActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar mainActivityToolbar = (Toolbar) findViewById(R.id.settings_activity_toolbar);
        setSupportActionBar(mainActivityToolbar);

        getFragmentManager().beginTransaction().replace(R.id.activity_settings_root_layout, new SettingsFragment()).commit();
    }


}
