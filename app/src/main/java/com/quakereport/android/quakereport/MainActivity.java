package com.quakereport.android.quakereport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    FetchResource fetchResource = null;
    public ListView mainActivityList;
    EarthquakeAdapter earthquakesAdapter;
    ArrayList<EarthquakeObject> earthquakeObjectList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar mainActivityToolbar = (Toolbar) findViewById(R.id.main_activity_toolbar);
        setSupportActionBar(mainActivityToolbar);
        PreferenceManager.setDefaultValues(this, R.xml.settings_main, false);

        mainActivityList = (ListView) findViewById(R.id.main_activity_list);


        earthquakeObjectList = new ArrayList<EarthquakeObject>();


        earthquakesAdapter = new EarthquakeAdapter(this, R.layout.main_activity_list_template, R.id.earthquake_magnitude_template_txt, earthquakeObjectList);


        mainActivityList.setAdapter(earthquakesAdapter);


        mainActivityList.setOnItemClickListener((new AdapterView.OnItemClickListener() {

            @Override

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EarthquakeObject currentObject = (EarthquakeObject) mainActivityList.getItemAtPosition(position);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(currentObject.getUrl()));

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }

            }
        }));

        fetchResource = new FetchResource();
        fetchResource.execute();
    }

    class FetchResource {


        private void execute() {
            RequestQueue queue = Volley.newRequestQueue(getBaseContext());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, buildURL(), null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // Log.d("myapp", response+"");
                            Log.d("myapp", "download executed");
                            populateAdapter(response);

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO: Handle error

                        }
                    });

// Access the RequestQueue through your singleton class.

            queue.add(jsonObjectRequest);

        }

    }

    private void populateAdapter(JSONObject root) {


        try {
            // JSONObject root = new JSONObject(SAMPLE_JSON_RESPONSE);
            Log.d("myapp", "populateadapter called");
            JSONArray features = root.getJSONArray("features");

            for (int i = 0; i < features.length(); i++) {

                JSONObject properties = features.getJSONObject(i).getJSONObject("properties");
                Long timeAsLong = Long.parseLong(properties.getString("time"));

                EarthquakeObject earthquakeObject = new EarthquakeObject(properties.getString("place"), properties.getString("mag"), timeAsLong, properties.getString("url"));
                earthquakeObjectList.add(earthquakeObject);

            }
            Log.d("myapp", "iteration coomplete");

        } catch (JSONException e) {
            Log.d("myapp", "error caught");
            e.printStackTrace();
        }

        Log.d("myapp", "added");

        earthquakesAdapter.refresh(earthquakeObjectList);
       // mainActivityList.setAdapter(earthquakesAdapter);

    }

    public static int getAppropriateBackgroundColor(Double magnitude) {
        int appropriateBackgroundColor = 0;


        if (magnitude > 0 && magnitude <= 2) {

            appropriateBackgroundColor = R.color.magnitude1;
        } else if (magnitude > 2 && magnitude <= 3) {

            appropriateBackgroundColor = R.color.magnitude2;
        } else if (magnitude > 3 && magnitude <= 4) {

            appropriateBackgroundColor = R.color.magnitude3;
        } else if (magnitude > 4 && magnitude <= 5) {

            appropriateBackgroundColor = R.color.magnitude4;
        } else if (magnitude > 5 && magnitude <= 6) {
            appropriateBackgroundColor = R.color.magnitude5;

        } else if (magnitude > 6 && magnitude <= 7) {

            appropriateBackgroundColor = R.color.magnitude6;
        } else if (magnitude > 7 && magnitude <= 8) {
            appropriateBackgroundColor = R.color.magnitude7;

        } else if (magnitude > 8 && magnitude <= 9) {
            appropriateBackgroundColor = R.color.magnitude8;

        } else if (magnitude > 9 && magnitude <= 10) {
            appropriateBackgroundColor = R.color.magnitude9;

        } else {
            appropriateBackgroundColor = R.color.magnitude10plus;

        }


        return appropriateBackgroundColor;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                launchSettingsActivity();
                return true;
            case R.id.refresh_button:
                reloadList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void launchSettingsActivity(){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();
     /*   if(fetchResource!=null) {
            fetchResource.execute();
        }*/


    }

    public String buildURL(){

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        Uri defaultUrl = Uri.parse("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson")
                .buildUpon().appendQueryParameter("limit", sharedPref.getString("max_items","15"))
                .appendQueryParameter("orderby",sharedPref.getString("order_by","time") ).build();


        Log.d("myapp",defaultUrl.toString());

        return  defaultUrl.toString();

    }

    public void reloadList(){

        Log.d("myapp","reload button pressed");
          if(fetchResource!=null) {
              Log.d("myapp","fetch resource not null");
              earthquakesAdapter.clear();
            fetchResource.execute();
        }
    }

}

