package com.quakereport.android.quakereport;

import android.content.Context;
import android.graphics.Movie;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.R.id.list;
import static com.quakereport.android.quakereport.MainActivity.getAppropriateBackgroundColor;
import static com.quakereport.android.quakereport.R.menu.main;

/**
 * Created by ROJO on 16-Apr-18.
 */

public class EarthquakeAdapter extends ArrayAdapter<EarthquakeObject> {

   Context context;
    int templateLayout;
    int textId;
    ArrayList<EarthquakeObject> earthquakeObjectList;

    public EarthquakeAdapter(Context context, int templateLayout, int textId, ArrayList<EarthquakeObject> earthquakeObjectList) {
        super(context, 0 , earthquakeObjectList);
        this.context = context;
        this.templateLayout = templateLayout;
        this.textId = textId;
        this.earthquakeObjectList = earthquakeObjectList;


    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //View view = super.getView(position, convertView, parent);
           View view = convertView;
        if(view == null)
            view = LayoutInflater.from(context).inflate(R.layout.main_activity_list_template,parent,false);
        // LinearLayout linearLayout = (LinearLayout)view;
        TextView primaryLocationTxt = (TextView)view.findViewById(R.id.earthquake_primary_location_template_txt);
        TextView locationOffsetTxt = (TextView)view.findViewById(R.id.earthquake_location_offset_template_txt);
        TextView magnitudeTxt = (TextView)view.findViewById(R.id.earthquake_magnitude_template_txt);
        TextView date = (TextView)view.findViewById(R.id.earthquake_date_template_txt);
        TextView time = (TextView)view.findViewById(R.id.earthquake_time_template_txt);
        EarthquakeObject earthquakeObject =  getItem(position);

        String location = earthquakeObject.getLocation();
        String primaryLocation, locationOffset;
        if(location.contains(" of")) {
            primaryLocation = new String(location.substring(location.indexOf(" of")));
            primaryLocation = primaryLocation.substring(4);
            locationOffset = new String(location.substring(0, location.indexOf(" of")) +" "+ context.getString(R.string.of) );
        }
        else{
            primaryLocation = new String(location);
            locationOffset = new String(context.getString(R.string.nearThe));

        }
        locationOffsetTxt.setText(locationOffset);
        primaryLocationTxt.setText(primaryLocation);
        Double magnitude = Double.parseDouble(earthquakeObject.getMagnitude());
        DecimalFormat formatter = new DecimalFormat("0.0");
        String formattedMagnitude = formatter.format(magnitude);
        GradientDrawable magnitudeBackgroundCircle = (GradientDrawable) magnitudeTxt.getBackground();
        magnitudeBackgroundCircle.setColor(ContextCompat.getColor(context, getAppropriateBackgroundColor(magnitude)));
        magnitudeTxt.setText(formattedMagnitude);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy");
        Date dateObject = new Date(earthquakeObject.getTimeStamp());
        date.setText(simpleDateFormat.format(dateObject));
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("h:mm a");
        time.setText(simpleTimeFormat.format(dateObject));

        return view;
    }

    public void refresh(ArrayList<EarthquakeObject> earthquakeObjectList)
    {
        //this.earthquakeObjectList.clear();

        this.earthquakeObjectList = earthquakeObjectList;
        notifyDataSetChanged();
       // mainActivityList.invalidate();

    }


}
